import React, { useState, useRef } from 'react';
import { StyleSheet, Text, View, FlatList, Animated, TouchableOpacity, Dimensions, } from 'react-native';
import slides from'../slides';
import OnboardingItem from '../components/OnboardingItem';
import Paginator from '../components/Paginator';

export default Onboarding = () => {
    const { width } = Dimensions.get('window');
    const [currentIndex, setCurrentIndex] = useState (0);
    const ref = useRef(null);
    const scrollX = useRef(new Animated.Value(0)).current;

    const viewableItemsChanged = useRef(({viewableItems})=> {
        setCurrentIndex(viewableItems[0].index);
    }).current;

   const viewConfig = useRef({viewAreaCoveragePercentThreshold: 50}).current;

   const updateCurrentSlideIndex = (e) => {
    const contentOffsetX = e.nativeEvent.contentOffset.x;
    const currentIndex = Math.round(contentOffsetX / width);
    setCurrentIndex(currentIndex);
   };
   const goNextSlide = () => {
    const nextSlideIndex = currentIndex + 1;
    const offset = nextSlideIndex * width;
    ref?.current?.scrollToOffset({ offset });
  };
  const home = () => {
    const firstSlideIndex = slides.length - currentIndex;
    const offset = firstSlideIndex * currentIndex;
    ref?.current?.scrollToOffset({ offset });
    setCurrentIndex(firstSlideIndex);
  };

  return (
    <View style={styles.container}>
        <View style={{flex:3}}>
      <FlatList onMomentumScrollEnd={updateCurrentSlideIndex}
          data={slides} renderItem={({item}) => <OnboardingItem item={item} />}
      horizontal
      showsHorizontalScrollIndicator={false}
      pagingEnabled
      bounces={false}
      keyExtractor={(item) => item.id}
      onScroll={Animated.event([{nativeEvent: {contentOffset:{x:scrollX } } }],{
          useNativeDriver: false,
      })}
      scrollEventThrottle={32}
      onViewableItemsChanged={viewableItemsChanged}
      viewabilityConfig={viewConfig}
      ref={ref}
     />
    </View>
    <Paginator data={slides} scrollX={scrollX} />
    <View style={{ marginBottom: 50 }}>
    {currentIndex == slides.length - 1 ?
          (<View style={{ height: 50 }}>
            <TouchableOpacity style={[styles.button, { backgroundColor: 'transparent', borderWidth: 1 }]} onPress={home}>
              <Text style={{ fontWeight: 'bold', fontSize: 15 }}>
                Home</Text>
            </TouchableOpacity>
          </View>
          ) : (
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity style={[styles.button, { backgroundColor: 'transparent', borderWidth: 1 }]} onPress={goNextSlide}>
                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>
                  Continue</Text>
              </TouchableOpacity>
            </View>)
        }
    </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});