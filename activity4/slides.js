export default [
{
    id: '1',
    title: 'Why I Chose IT Course??',
    description: 'Firstly, Because This Is My Best Knowledgeable Course',
    image: require ('./assets/images/pic1.png'),
},
{
    id: '2',
    title: 'Second Answer',
    description: 'Because Computer Subject Was My Highest Grade on Past School Years',
    image: require ('./assets/images/pic2.png'),
},
{
    id: '3',
    title: 'Third Answer',
    description: 'My Parents Suggested Me To Get The Course I Am Familiar With',
    image: require ('./assets/images/pic3.png'),
},
{
    id: '4',
    title: 'Fourth Answer',
    description: 'Because Computer Subject Was My Highest Grade on Past School Years',
    image: require ('./assets/images/pic4.png'),
},
{
    id: '5',
    title: 'Last Answer',
    description: 'I Like To Know More About Computers',
    image: require ('./assets/images/pic5.png'),
},
];