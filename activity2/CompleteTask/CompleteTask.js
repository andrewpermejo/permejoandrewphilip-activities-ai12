import * as React from 'react';
import { View, Text } from 'react-native';


export default function CompleteTask({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'aqua' }}>
      <Text
        onPress={() => alert('Completed Task Go Here')}
        style={{ fontSize: 30, fontWeight: 'bold' }}>Completed Task Go Here</Text>
    </View>
  );
}